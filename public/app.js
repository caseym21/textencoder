var text = document.getElementById('text-btn')
var bin = document.getElementById('binary-btn')
var rot13 = document.getElementById('rot13-btn')
var hex = document.getElementById('hex-btn')
var decimal = document.getElementById('decimal-btn')
var base64 = document.getElementById('base64-btn')

text.addEventListener('click', ()=>{
    textInput = document.getElementById('text-input').value
    binInput = document.getElementById('bin-input')
    rot13Input = document.getElementById('rot13-input')
    hexInput = document.getElementById('hex-input')
    decimalInput = document.getElementById('decimal-input')
    base64Input = document.getElementById('base64-input')

    axios.post('/encoding-text', {textInput: textInput}).then((response) =>{
        binInput.value = response.data.binEncrypt;
        rot13Input.value = response.data.rot13Encrypt;
        hexInput.value = response.data.hexEncrypt;
        decimalInput.value = response.data.decimalEncrypt;
        base64Input.value = response.data.base64Encrypt;
    })
})

bin.addEventListener('click', ()=>{
    binInput = document.getElementById('bin-input').value
    textInput = document.getElementById('text-input')
    rot13Input = document.getElementById('rot13-input')
    hexInput = document.getElementById('hex-input')
    decimalInput = document.getElementById('decimal-input')
    base64Input = document.getElementById('base64-input')

    axios.post('/encoding-bin', {binInput: binInput}).then((response) =>{
        textInput.value = response.data.text;
        rot13Input.value = response.data.rot13Encrypt;
        hexInput.value = response.data.hexEncrypt;
        decimalInput.value = response.data.decimalEncrypt;
        base64Input.value = response.data.base64Encrypt;
    })
})

rot13.addEventListener('click', ()=>{
    rot13Input = document.getElementById('rot13-input').value
    textInput = document.getElementById('text-input')
    binInput = document.getElementById('bin-input')
    hexInput = document.getElementById('hex-input')
    decimalInput = document.getElementById('decimal-input')
    base64Input = document.getElementById('base64-input')

    axios.post('/encoding-rot13', {rot13Input: rot13Input}).then((response) =>{
        textInput.value = response.data.text;
        binInput.value = response.data.binEncrypt;
        hexInput.value = response.data.hexEncrypt;
        decimalInput.value = response.data.decimalEncrypt;
        base64Input.value = response.data.base64Encrypt;
    })
})

hex.addEventListener('click', ()=>{
    hexInput = document.getElementById('hex-input').value
    textInput = document.getElementById('text-input')
    binInput = document.getElementById('bin-input')
    rot13Input = document.getElementById('rot13-input')
    decimalInput = document.getElementById('decimal-input')
    base64Input = document.getElementById('base64-input')

    axios.post('/encoding-hex', {hexInput: hexInput}).then((response) =>{
        textInput.value = response.data.text;
        binInput.value = response.data.binEncrypt;
        rot13Input.value = response.data.rot13Encrypt;
        decimalInput.value = response.data.decimalEncrypt;
        base64Input.value = response.data.base64Encrypt;
    })
})

decimal.addEventListener('click', ()=>{
    decimalInput = document.getElementById('decimal-input').value
    textInput = document.getElementById('text-input')
    binInput = document.getElementById('bin-input')
    rot13Input = document.getElementById('rot13-input')
    hexInput = document.getElementById('hex-input')
    base64Input = document.getElementById('base64-input')

    axios.post('/encoding-decimal', {decimalInput: decimalInput}).then((response) =>{
        textInput.value = response.data.text;
        binInput.value = response.data.binEncrypt;
        rot13Input.value = response.data.rot13Encrypt;
        hexInput.value = response.data.hexEncrypt;
        base64Input.value = response.data.base64Encrypt;
    })
})

base64.addEventListener('click', ()=>{
    base64Input = document.getElementById('base64-input').value
    textInput = document.getElementById('text-input')
    binInput = document.getElementById('bin-input')
    rot13Input = document.getElementById('rot13-input')
    hexInput = document.getElementById('hex-input')
    decimalInput = document.getElementById('decimal-input')

    axios.post('/encoding-base64', {base64Input: base64Input}).then((response) =>{
        textInput.value = response.data.text;
        binInput.value = response.data.binEncrypt;
        rot13Input.value = response.data.rot13Encrypt;
        hexInput.value = response.data.hexEncrypt;
        decimalInput.value = response.data.decimalEncrypt;
    })
})
