const express = require('express')
const server = express()

server.use(express.static('public'))
server.use(express.json())

function toBinary(string){
    var binEncrypt = [];
    for (var i = 0;i < string.length; i++) {
        var bin = string[i].charCodeAt().toString(2);
        binEncrypt.push(Array(8-bin.length+1).join("0") + bin);
    }
    binEncrypt = binEncrypt.join(" ")
    return binEncrypt
}

function fromBinary(string){
    var binDecrypt = '';
    string.split(' ').map(function(bin) {
        binDecrypt += String.fromCharCode(parseInt(bin, 2));
    });
    return binDecrypt
}

function toRot13(string){
    return string.replace(/[A-Z]/gi, c =>
        "NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm"["ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".indexOf(c)])
} 

function fromRot13(string){
    return string.replace(/[A-Z]/gi, c =>
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"["NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm".indexOf(c)])
} 

function toDecimal(string){
    var decimalEncrypt = '';
    for(var i = 0; i < string.length; i++){
        decimalEncrypt += (string.charCodeAt(i) + " ") 
    }

    return decimalEncrypt
}

function fromDecimal(string){
    var decimalDecrypt = '';
    var decimalArray = [];
    decimalArray = string.split(" ")

    for(var i = 0; i < decimalArray.length; i++){
        decimalDecrypt += String.fromCharCode(decimalArray[i])
    }

    return decimalDecrypt;
}

function toHex(string){
    var string, i;
    var hexEncrypt = '';
    for(var i = 0; i<string.length; i++) {
      hexEncrypt += (string.charCodeAt(i).toString(16) + " ");
    }
    hexEncrypt[hexEncrypt.length] = "";

    return hexEncrypt;
}

function fromHex(string) {
    string = string.toString().replace(/\s+/gi, '')
    var hexDecrypt = []

    for (var i = 0; i < string.length; i += 2) {
      var hex = parseInt(string.substr(i, 2), 16)
        hexDecrypt.push(String.fromCharCode(hex))
    }
    hexDecrypt = hexDecrypt.join('')

    return hexDecrypt
}  

function toBase64(string){
    var base64Encrypt = Buffer.from(string).toString('base64')
    return base64Encrypt
}

function fromBase64(string){
    var base64Decrypt = Buffer.from(string, 'base64').toString('ascii')
    return base64Decrypt
}

function textToAll(string){
    var text = string;
    var binEncrypt = toBinary(string);
    var rot13Encrypt = toRot13(string);
    var hexEncrypt = toHex(string);
    var decimalEncrypt = toDecimal(string);
    var base64Encrypt = toBase64(string);

    return({text, binEncrypt, rot13Encrypt, hexEncrypt, decimalEncrypt, base64Encrypt})
}

server.get('/*', (req,res)=>{
    res.sendFile(__dirname + '/public/index.html');
})

server.post('/encoding-text', (req,res)=>{
    var input = req.body.textInput;
    res.send(textToAll(input));
})

server.post('/encoding-bin', (req,res)=>{
    var input = req.body.binInput;
    var binDecrypt = fromBinary(input)
    res.send(textToAll(binDecrypt))
})

server.post('/encoding-rot13', (req,res)=>{
    var input = req.body.rot13Input
    var rot13Decrypt = fromRot13(input)
    res.send(textToAll(rot13Decrypt))
})

server.post('/encoding-hex', (req,res)=>{
    var input = req.body.hexInput
    var hexDecrypt = fromHex(input)
    res.send(textToAll(hexDecrypt))
})

server.post('/encoding-decimal', (req,res)=>{
    var input = req.body.decimalInput
    var decimalDecrypt = fromDecimal(input)
    res.send(textToAll(decimalDecrypt))
})

server.post('/encoding-base64', (req,res)=>{
    var input = req.body.base64Input
    var base64Decrypt = fromBase64(input)
    res.send(textToAll(base64Decrypt))
})

server.listen(3001)